﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shulga.TestWork.T9Spelling
{
    class Program
    {
        static void Main(string[] args)
        {
            Phone p = new Phone();
            //Console.WriteLine( p.WriteText("hel.lo world"));
            List<string> stingList = new List<string>();
            int N = 0;
            
            //chek N
            do
            {
                Console.Clear();
                Console.WriteLine("Write number of tests: ");
                N =Convert.ToInt32(Console.ReadLine());

            } while (N < 0 || N > 100);


            //input
            for (int i = 0; i < N; i++)
            {
                Console.Write("Enter "+(i+1)+" word: ");
                stingList.Add(Console.ReadLine());
            }
            //output
            for (int i = 0; i < N; i++)
             Console.WriteLine("Case #" + (i + 1) + " " + p.WriteText(stingList[i]));
            
        }
    }
}
