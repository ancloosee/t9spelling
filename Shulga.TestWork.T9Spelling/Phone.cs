﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shulga.TestWork.T9Spelling
{
   public class Phone
    {
        KeyBase KeyBases;


        public Phone()
        {
            KeyBases = new KeyBase();
        }


        public string WriteText(string text)
        {
            //for save rezult
            StringBuilder rezult = new StringBuilder("");
            //for save previus numberkey 
            char previusNumber = ' ';

            
            char[] symbolList = text.ToCharArray();
            
            for (int i = 0; i < symbolList.Length; i++)
            {
                //if need print whitespace
                if(previusNumber== KeyBases.GetNumber(symbolList[i]))
                    rezult.Append(' ', 1);
                //other symbols like *,?,! will ignore
                if (KeyBases.GetNumber(symbolList[i]) == '*') continue; 
                //add to rezult
                rezult.Append(KeyBases.GetNumber(symbolList[i]), KeyBases[symbolList[i]]);
                //save previous keynumber
                previusNumber = KeyBases.GetNumber(symbolList[i]);
            }

            return rezult.ToString();
        }
    }
}
