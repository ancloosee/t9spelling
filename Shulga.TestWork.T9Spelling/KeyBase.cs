﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shulga.TestWork.T9Spelling
{
  public  class KeyBase
    {

        //
        Dictionary<char, List<char>> KeyBoard;


        //default constructor
        public KeyBase()
        {
            KeyBoard = new Dictionary<char, List<char>>();
            KeyBoard.Add('1', new List<char> { });
            KeyBoard.Add('2', new List<char> { 'a', 'b', 'c' });
            KeyBoard.Add('3', new List<char> { 'd', 'e', 'f' });
            KeyBoard.Add('4', new List<char> { 'g', 'h', 'i' });
            KeyBoard.Add('5', new List<char> { 'j', 'k', 'l' });
            KeyBoard.Add('6', new List<char> { 'm', 'n', 'o' });
            KeyBoard.Add('7', new List<char> { 'p', 'q', 'r','s' });
            KeyBoard.Add('8', new List<char> { 't', 'u', 'v' });
            KeyBoard.Add('9', new List<char> { 'w', 'x', 'y','z' });
            KeyBoard.Add('0', new List<char> { ' ','+' });
        }

        //indexator for return return  number of keystrokes to receive a symbol
        public int this[char symbol]
        {
            get
            {
                
                if (char.IsLetter(symbol))
                {
                 //if upper case,change to lower,because it does not matter
                    if (char.IsUpper(symbol))
                        symbol = char.ToLower(symbol);
                    //search symbol and return  number of keystrokes to receive a symbol
                     if (symbol <= 'c')
                        return KeyBoard['2'].IndexOf(symbol) + 1;
                    else if (symbol <= 'f')
                        return KeyBoard['3'].IndexOf(symbol) + 1;
                    else if (symbol <= 'i')
                        return KeyBoard['4'].IndexOf(symbol) + 1;
                    else if (symbol <= 'l')
                        return KeyBoard['5'].IndexOf(symbol) + 1;
                    else if (symbol <= 'o')
                        return KeyBoard['6'].IndexOf(symbol) + 1;
                    else if (symbol <= 's')
                        return KeyBoard['7'].IndexOf(symbol) + 1;
                    else if (symbol <= 'v')
                        return KeyBoard['8'].IndexOf(symbol) + 1;
                    else if (symbol <= 'z')
                        return KeyBoard['9'].IndexOf(symbol) + 1;
                    else return 0;
                }
                else if (symbol == ' ' || symbol == '+')
                    return KeyBoard['0'].IndexOf(symbol) + 1;
                ////if we find other sumbol like *,!,? and etc. We return 0
                else return 0;
            }
        }

        public char GetNumber(char symbol)
        {
            if (char.IsLetter(symbol))
            {    
                //if upper case,change to lower,because it does not matter
                if (char.IsUpper(symbol))
                    symbol = char.ToLower(symbol);

                //search symbol and return number in the keyboard
                 if (symbol <= 'c')
                    return '2';
                else if (symbol <= 'f')
                    return '3';
                else if (symbol <= 'i')
                    return '4';
                else if (symbol <= 'l')
                    return '5';
                else if (symbol <= 'o')
                    return '6';
                else if (symbol <= 's')
                    return '7';
                else if (symbol <= 'v')
                    return '8';
                else if (symbol <= 'z')
                    return '9';
                else return '*';
            }
            else if (symbol == ' ' || symbol == '+')
                return '0';
            //if we find other sumbol like *,!,? and etc. We return '*'and them if we find other sumbol like *,!,? and etc. We return '*'and them check it and do something, for example ignore it and do something, for example ignore 
            else return '*';
        }
    }
}
