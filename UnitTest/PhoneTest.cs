﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Shulga.TestWork.T9Spelling;

namespace UnitTest
{
    [TestClass]
    public class PhoneTest
    {
        [TestMethod]
        public void TestInput_hi_Output_33_444()
        {
            Assert.AreEqual("44 444", new Phone().WriteText("hi"));
        }
        [TestMethod]
        public void TestInput_HI_Output_33_333()
        {
            Assert.AreEqual("44 444", new Phone().WriteText("HI"));
        }
        [TestMethod]
        public void TestInput_Hi_Output_33_333()
        {
            Assert.AreEqual("44 444", new Phone().WriteText("Hi"));
        }

        [TestMethod]
        public void TestInput_yes_Output_999337777()
        {
            Assert.AreEqual("999337777", new Phone().WriteText("yes"));
        }
        [TestMethod]
        public void TestInput_Yes_Output_999337777()
        {
            Assert.AreEqual("999337777", new Phone().WriteText("Yes"));
        }
        [TestMethod]
        public void TestInput_YeS_Output_999337777()
        {
            Assert.AreEqual("999337777", new Phone().WriteText("YeS"));
        }

        [TestMethod]
        public void TestInput_foo_bar_Output_333666_6660_022_2777()
        {
            Assert.AreEqual("333666 6660 022 2777", new Phone().WriteText("foo  bar"));
        }


        [TestMethod]
        public void TestInput_hello_world_Output_4433555_555666096667775553()
        {
            Assert.AreEqual("4433555 555666096667775553", new Phone().WriteText("hello world"));
        }

        [TestMethod]
        public void TestInput_TABLE_Output_82_2255533()
        {
            Assert.AreEqual("82 2255533", new Phone().WriteText("TABLE"));
        }

        [TestMethod]
        public void TestInput_hell_DOT_o_world_Output_4433555_555666096667775553()
        {
            Assert.AreEqual("4433555 555666096667775553", new Phone().WriteText("hel.lo world"));
        }
        [TestMethod]
        public void TestInput_hell_QS_o_world_Output_4433555_555666096667775553()
        {
            Assert.AreEqual("4433555 555666096667775553", new Phone().WriteText("hel?lo w?orl?d"));
        }
    }
}
