﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Shulga.TestWork.T9Spelling;

namespace UnitTest
{
    [TestClass]
    public class KeyBaseTest
    {
        [TestMethod]
        public void TestInput_c_Output_3()
        {
            Assert.AreEqual(3,new KeyBase()['c']);
        }
        [TestMethod]
        public void TestInput_C_Output_3()
        {
            Assert.AreEqual(3, new KeyBase()['C']);
        }
        [TestMethod]
        public void TestInput_K_Output_2()
        {
            Assert.AreEqual(2, new KeyBase()['K']);
        }
        [TestMethod]
        public void TestInput_k_Output_2()
        {
            Assert.AreEqual(2, new KeyBase()['k']);
        }
        [TestMethod]
        public void TestInput_WHITESPASE_Output_2()
        {
            Assert.AreEqual(1, new KeyBase()[' ']);
        }
    }
}
